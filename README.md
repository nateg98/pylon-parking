# Pylon Parking
### Systems and Networking Capstone Spring 2020

A blockchain solution for reserving parking spots

![Make me a sandwich](media/sandwich.png)

If you are cloning into something for the first time, go to the folder with the  
package lock and ```npm install```, also do this if you pull a new version and  
need to install a new module